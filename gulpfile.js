'use strict';

var browserify    = require('browserify');
var gulp          = require('gulp');
var clean         = require('gulp-clean');
var source        = require('vinyl-source-stream');
var buffer        = require('vinyl-buffer');
var uglify        = require('gulp-uglify');
var sourcemaps    = require('gulp-sourcemaps');
var jshint        = require('gulp-jshint');
var sass          = require('gulp-sass');
var postcss       = require('gulp-postcss');
var autoprefixer  = require('autoprefixer');
var cssnano       = require('cssnano');
var concat        = require('gulp-concat');
var rename        = require('gulp-rename');
var browserSync   = require('browser-sync').create();

gulp.task('clean-build', function () {
  return gulp.src(['./build/**/*.js', './build/**/*.css', './build/**/*.map'], {read: false})
    .pipe(clean());
});

gulp.task('js-lint', function () {
  return gulp.src('src/js/**/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('js', ['js-lint'], function() {
  return browserify({
      entries: ['./src/js/index.js'],
      debug: true,
      transform: ['brfs', 'envify']
    })
    .bundle()
    .on("error", function (error) {
      console.log("Error : " + error.message);
    })
    .pipe(source('traildock.min.js'))
    .pipe(buffer())
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(uglify())
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./build'));
});

gulp.task('css', function() {
  var processors = [
    autoprefixer({ browsers: ["last 3 versions"] }),
    cssnano()
  ];
  return gulp.src(['src/scss/index.scss'])
    .pipe(rename('traildock.min.css'))
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(postcss(processors))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./build'));
});

gulp.task('watch', ['js', 'css'], function () {
  gulp.watch(['src/js/**/*.js', 'src/config/*.json', 'src/js/**/*.html'], ['js']);
  gulp.watch(['src/scss/**/*.scss'], ['css']);
});

gulp.task('js-watch', ['js'], function(done) {
  browserSync.reload();
  done();
});

gulp.task('css-watch', ['css'], function(done) {
  browserSync.reload();
  done();
});

gulp.task('develop', ['js', 'css'], function () {
  browserSync.init({
    server: ['.', './build'],
    ui: {
      port: 3003
    },
    port: 3002
  });
  gulp.watch(['src/js/**/*'], ['js-watch']);
  gulp.watch(['src/scss/**/*.scss'], ['css-watch']);
});
gulp.task('build', ['clean-build', 'css', 'js'])
gulp.task('default', ['build']);
