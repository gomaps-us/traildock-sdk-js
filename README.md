# Traildock Javascript SDK

## Dependencies

* Node >= 9
* [Yarn](https://yarnpkg.com/en/docs/install). You can use npm if you like locally, but the preferred method is Yarn, and it's what we use for dependency installation and script execution in other environments.

## Developing

1. Get the Traildock crypt key from an admin and place it in the root of your clone, in a file named `.cryptkey`
2. Run the following:
```
yarn install
yarn run decrypt-configs
yarn run develop
```

The following should load the web interactive map and other browser-related development environment items. You can also access it manually at http://localhost:3002.

## Testing

No tests written for this project yet

## Using the SDK in the Browser

This SDK can be used in the browser as client-side javascript, or as a NodeJS module (npm package coming soon).

To use the Javascript SDK in your web site or application, simply add the script and css tags to your page or pages:

```
<html>
  <head>
    ...
    <link rel="stylesheet" type="text/css" href="//cdn.traildock.com/sdks/js/stable.min.css" />
  </head>
  <body>
    ...
    <script type="text/javascript" src="//cdn.traildock.com/sdks/js/stable.min.js"></script>
  </body>
</html>
```

## Web Interactive Map

To embed the web interactive map into your web site or application, add the following to your html body:

```
<div id="traildock-map"
     width="100%"
     height="500px"
     data-latitude="38.869715"
     data-longitude="-106.987823"
     data-zoom="11"
     data-locate="yes"
     data-api-url="https://api.traildockqa.com"
     data-tiles="google"></div>
```

### Options

| option name       | description                                                                                                                                                 | default value                  |
|-------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------ |--------------------------------|
| `width`           | you can specify the width of the map container                                                                                                              | `100%`                         |
| `height`          | you can specify the height of the map container                                                                                                             | `400px`                        |
| `data-latitude`   | the starting center latitude point of the                                                                                                                   | `38.869715`                    |
| `data-longitude`  | the starting center longitude point of the map                                                                                                              | `-106.987823`                  |
| `data-zoom`       | the starting zoom level of the map                                                                                                                          | `15`                           |
| `data-locate`     | if set to false/no, then location services will be turned off, no attempts to determine the location of the current user                                    | `yes`                          |
| `data-api-url`    | if you want to use a different base url for the data api, useful for testing                                                                                | `https://api.traildock.com`    |
| `data-tiles`      | the default map background layer, possible values: `traildock`, `google`, `usgs-topo`, `google-satellite`                                                   | `traildock`                    |

## Deploying

### Configuration

Configuration lives in `/config/*`. Config files are encrypted since they contain sensitive info like creds for connecting to other services. Make sure you run the `yarn run decrypt-configs` command mentioned above after placing the `.cryptkey` to be able to deploy/release

### Releasing

Just `yarn run release`. This will take the necessary `/build/{*.js,*.css}` files and release then to S3/the CDN.
