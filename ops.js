'use strict';

var winston     = require('winston-color');
var ops         = require('traildock-node-library').ops;
var path        = require('path');
var fs          = require('fs');
var config      = require('config');
var packageJs   = require('./package.json');

var command = process.argv.splice(2);
winston.info('Running ' + command.join(' '));

if (command[0] == 'crypt' && command.length > 0 && command[1] == 'encrypt-configs') {
  ops.crypt(path.resolve(__dirname)).encryptConfigs();
}
if (command[0] == 'crypt' && command.length > 0 && command[1] == 'decrypt-configs') {
  ops.crypt(path.resolve(__dirname)).decryptConfigs();
}

var s3Upload = function (aws, type, contentType) {
  var s3 = new aws.S3();
  var stream = fs.createReadStream(path.resolve(__dirname, 'build', 'traildock.min.' + type));
  stream.on('error', function (err) {
    if (err) throw err;
  });
  stream.on('open', function () {
    s3.putObject({
      Bucket: config.get('aws.bucket'),
      Key: config.get('aws.path') + '/' + packageJs.version + '.min.' + type,
      Body: stream,
      ContentType: contentType
    }, function (err) {
      if (err) throw err;
      winston.info('uploaded ' + type + ' as version file');
    });
    s3.putObject({
      ACL: 'public-read',
      Bucket: config.get('aws.bucket'),
      Key: config.get('aws.path') + '/stable.min.' + type,
      Body: stream,
      ContentType: contentType
    }, function (err) {
      if (err) throw err;
      winston.info('uploaded ' + type + ' as stable file');
    });
  });
}

if (command[0] == 'release') {
  process.env.AWS_ACCESS_KEY_ID       = config.get('aws.accessKeyId');
  process.env.AWS_SECRET_ACCESS_KEY   = config.get('aws.secretAccessKey');
  process.env.AWS_REGION              = config.get('aws.region');
  var AwsSdk                          = require('aws-sdk');

  s3Upload(AwsSdk, 'js', 'application/javascript');
  s3Upload(AwsSdk, 'css', 'text/css');
}
