'use strict';

var leaflet = require('leaflet');
var cookies = require('browser-cookies');

module.exports = function(sdk) {

  var mapElement;

  if (!document) {
    throw 'The Traildock map can only be used in a browser, client-side javascript environment';
  }

  if (sdk.utils.getIEVersion() !== false) {
    mapElement = document.getElementById('traildock-map');
    mapElement.setAttribute('style', 'min-height:' + mapElement.getAttribute('height') + ';');
    sdk.utils.addClass(mapElement, 'disabled');
    mapElement.innerHTML = "<div>Sorry, looks like you're using Internet Explorer or Edge. \
                            We've not been able to ensure full support with these browsers yet. \
                            If you want to use this <a href=\"https://traildock.com\">Traildock map</a>, \
                            please switch to another browser like Chrome or Firefox. Assuming you can make \
                            the switch, we guarantee you won't regret it. Otherwise, we'll have something \
                            working for IE and Edge soon. Thanks!</div>";
    return;
  }

  var _this               = this;
  this.mapObject          = null;
  this.drawerButtonsElem  = null;
  this.mapElement         = null;

  this.content            = new (require('./content'))(sdk);
  this.gpsMarker          = null;

  this.drawers = {
    account: new (require('./content/drawers/account'))(sdk, this.content),
    contribute: new (require('./content/drawers/contribute'))(sdk, this.content),
    settings: new (require('./content/drawers/settings'))(sdk, this.content),
    legend: new (require('./content/drawers/legend'))(sdk, this.content),
    help: new (require('./content/drawers/help'))(sdk, this.content)
  };
  if (sdk.utils.isMobile()) {
    this.drawers.contribute = new (require('./content/drawers/photo'))(sdk, this.content);
  }

  this.tileLayers   = {};
  this.markers      = [];
  this.activeMarker = null;

  this.traildockLogoUrl = 'https://cdn.traildock.com/images/traildock-text.png';

  this.init = function () {
    _this.mapElement.querySelector('.loading').style.display = 'block';
    sdk.api.init(_this.mapElement.getAttribute('data-api-url') ? _this.mapElement.getAttribute('data-api-url') : null).then(function () {
      sdk.utils.loadStyle('//unpkg.com/leaflet@' + leaflet.version + '/dist/leaflet.css').then(function () {
        return sdk.utils.loadStyle('//fonts.googleapis.com/icon?family=Material+Icons');
      }).then(function () {
        _this.mapObject = leaflet.map(mapElement.id, {
          markerZoomAnimation: true
        });
        _this.mapObject.on('load', function() {
          setTimeout(function () {
            sdk.utils.addClass(_this.mapElement, 'loaded');
          }, 500);
        });
        for (var key in sdk.config.map.layers.tiles) {
          if (sdk.config.map.layers.tiles.hasOwnProperty(key)) {
            if (key === 'active') continue;
            var layer = sdk.config.map.layers.tiles[key];
            _this.tileLayers[key] = {
              name: layer.name,
              layer: leaflet.tileLayer(layer.url, { attribution: layer.attribution }),
              active: false
            };
          }
        }
        _this.activateTileLayer(sdk.config.map.layers.tiles.active);

        // add drawers
        for (var drawerName in _this.drawers) {
          if (_this.drawers.hasOwnProperty(drawerName)) {
            if (sdk.config.map.drawers[drawerName].enabled) _this.addDrawer(_this.drawers[drawerName]);
          }
        }
        _this.toggleDrawerButtons();
        _this.mapElement.querySelector('.drawer-buttons').style.right = '0';

        var redraw = sdk.utils.debounce(function () {
          if (_this.activeMarker) return;
          _this.drawContent();
          if (_this.mapObject.getCenter().lat) cookies.set('traildock-map-latitude', _this.mapObject.getCenter().lat.toString());
          if (_this.mapObject.getCenter().lng) cookies.set('traildock-map-longitude', _this.mapObject.getCenter().lng.toString());
          if (_this.mapObject.getZoom() >= 0) cookies.set('traildock-map-zoom', _this.mapObject.getZoom().toString());
          _this.updateHash();
        }, 500);

        _this.mapObject.on('zoomend', redraw);
        _this.mapObject.on('moveend', redraw);
        _this.mapObject.on('popupopen', function (event) {
          _this.activeMarker = event.popup._source;
        });
        _this.mapObject.on('popupclose', function (event) {
          _this.activeMarker = null;
        });

        var viewInit = function (navigatorPosition) {
          var latitude, longitude, zoom, hashView;
          if(window.location.hash) {
            var hashParams  = sdk.utils.parseUrlParams(window.location.hash.toString().substring(1));
            hashView        = hashParams.view.split('/');
            zoom            = hashView[0];
            latitude        = hashView[1];
            longitude       = hashView[2];
          } else {
            latitude  = cookies.get('traildock-map-latitude') ? cookies.get('traildock-map-latitude') : sdk.config.map.latitude;
            longitude = cookies.get('traildock-map-longitude') ? cookies.get('traildock-map-longitude') : sdk.config.map.longitude;
            zoom      = cookies.get('traildock-map-zoom') ? cookies.get('traildock-map-zoom') : sdk.config.map.zoom;
          }
          _this.setView([latitude, longitude], zoom);
          if (navigatorPosition) {
            var centerLocation = !hashView && (!cookies.get('traildock-map-latitude') || !cookies.get('traildock-map-longitude'));
            _this.updateUserPosition([navigatorPosition.coords.latitude, navigatorPosition.coords.longitude], centerLocation);
          }
          _this.mapElement.querySelector('.loading').style.display = 'none';
          _this.registerEventListeners();
        };

        if (sdk.config.map.locate && navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function (position) {
            _this.addDrawerButton({
              name: "gps",
              svgIcon: _this.content.gps.iconFixed,
              title: "Go to my location",
              action: "window.traildock.map.goToUserPosition();"
            });
            viewInit(position);
            navigator.geolocation.watchPosition(function (position) {
              _this.updateUserPosition([position.coords.latitude, position.coords.longitude], false);
            });
          }, function (error) {
            console.error('Error getting geo location in browser', error);
            viewInit();
          });
        } else {
          viewInit();
        }

      });
    });
  };

  this.updateHash = function () {
    var hash = '#view=' + _this.mapObject.getZoom() + '/' + _this.mapObject.getCenter().lat + '/' + _this.mapObject.getCenter().lng;
    if (history.pushState) {
      history.pushState(null, null, hash);
    } else {
      location.hash = hash;
    }
  };

  this.setView = function (coords, zoom, animate) {
    var  options = {};
    if (animate === true) {
      options.animate = true;
      options.pan = {
        duration: 3
      };
    }
    if (!zoom) zoom = _this.mapObject.getZoom();
    _this.mapObject.setView(coords, zoom);
  };

  this.updateUserPosition = function (coords, resetView) {
    if (!_this.gpsMarker) {
      _this.gpsMarker = new _this.content.gps.layer(coords).addTo(_this.mapObject);
    } else {
      _this.gpsMarker.setLatLng(coords);
    }
    if (resetView === true) {
      _this.setView(coords, _this.mapObject.getZoom(), true);
    }
  };

  this.goToUserPosition = function () {
    _this.setView(_this.gpsMarker.getLatLng(), _this.mapObject.getZoom(), true);
  };

  this.addMarker = function (type, data) {
    var view  = _this.content.createView(type, data);
    var popup = leaflet.popup({
      closeButton: false
    }).setContent(view.html);
    var marker = leaflet.marker([data.geometry.coordinates[1], data.geometry.coordinates[0]], {
      icon: leaflet.icon({
        iconUrl: view.icon.url,
        iconSize: [view.icon.size, view.icon.size]
      }),
      apiData: data
    });
    popup.on('add', function () {
      var editMenuTrigger = popup._container.querySelector('.edit-menu-trigger');
      if (editMenuTrigger && editMenuTrigger.addEventListener) {
        editMenuTrigger.addEventListener('click', function (event) {
          sdk.utils.addClass(popup._container.querySelector('.edit-menu'), 'open');
        });
        popup._container.querySelectorAll('.edit-menu li a').forEach(function (a) {
          a.addEventListener('click', function (event) {
            _this.content.edit(popup, type, event.target.getAttribute('data-edit'));
          });
        });
      }
    });
    this.markers.push(marker.bindPopup(popup).addTo(_this.mapObject));
  };

  this.clearMarkers = function () {
    for (var i = 0; i < this.markers.length; i++) {
      _this.mapObject.removeLayer(this.markers[i]);
    }
    this.markers = [];
  };

  this.getMarkerIds = function () {
    var ids = [];
    for (var i = 0; i < this.markers.length; i++) {
      ids.push(this.markers[i].options.apiData.id);
    }
    return ids;
  };

  this.removeMarkerById = function (id) {
    for (var i = 0; i < this.markers.length; i++) {
      if (this.markers[i].options.apiData.id === id) {
        _this.mapObject.removeLayer(this.markers[i]);
        this.markers.splice(i, 1);
        break;
      }
    }
  };

  this.drawContent = function () {
    if (_this.mapObject.getZoom() <= 9) {
      _this.clearMarkers();
      return;
    }
    var bounds = _this.mapObject.getBounds();
    var boundParameters = '?southWestLatitude=' + bounds.getSouthWest().lat +
                          '&southWestLongitude=' + bounds.getSouthWest().lng +
                          '&northEastLatitude=' + bounds.getNorthEast().lat +
                          '&northEastLongitude=' + bounds.getNorthEast().lng;
    var markerIds = _this.getMarkerIds();
    var i, ii;
    if (markerIds.length > 0) {
      boundParameters += '&filter={"where": {"id": {"nin": ["' + markerIds.join('","') + '"]}}}';
    }
    var types = [
      'report',
      'trailhead',
      'sign',
      'facility',
      'observation',
      'feature'
    ];
    var collections = types.map(function (type) {
      return _this.content[type].name;
    });
    sdk.api.request('GET', '/aggregate/' + collections.join(',') + '/within' + boundParameters, null, { silent: true }).then(function (response) {
      if (response.body instanceof Array) {
        if (response.body.length) {
          for (i = 0; i < response.body.length; i++) {
            for (ii = 0; ii < response.body[i].length; ii++) {
              if (response.body[i][ii].geometry) {
                _this.addMarker(types[i], response.body[i][ii]);
              }
            }
          }
        }
      }
    });
  };

  this.activateTileLayer = function (name) {
    // first let's see if we need to remove a layer (tile layer already active)
    for (var key in _this.tileLayers) {
      if (_this.tileLayers.hasOwnProperty(key)) {
        if (_this.tileLayers[key].active) {
          _this.mapObject.removeLayer(_this.tileLayers[key].layer);
          _this.tileLayers[key].active = false;
          break;
        }
      }
    }
    // then we add the layer
    _this.mapObject.addLayer(_this.tileLayers[name].layer);
    _this.tileLayers[name].active = true;
    var expires = new Date();
    expires.setSeconds(expires.getSeconds() + 1209600); // 2 weeks
    cookies.set('traildock-map-tiles', name, { expires: expires });

    _this.toggleDrawerButtons();
  };

  this.getActiveTileLayer = function () {
    for (var key in _this.tileLayers) {
      if (_this.tileLayers.hasOwnProperty(key)) {
        if (_this.tileLayers[key].active) return key;
      }
    }
  };

  this.toggleDrawerButtons = function () {
    if (_this.mapElement.querySelector('.drawer-buttons .legend')) {
      if (_this.getActiveTileLayer() === 'traildock') {
        _this.mapElement.querySelector('.drawer-buttons .legend').style.display = "block";
      } else {
        _this.mapElement.querySelector('.drawer-buttons .legend').style.display = "none";
      }
    }
  };

  this.registerEventListeners = function () {
    sdk.utils.listenForEvent(sdk.events.REQUEST_STARTED, function (data) {
      if (data.detail && ('silent' in data.detail) && data.detail.silent === true) return;
      _this.mapElement.querySelector('.loading').style.display = 'block';
    });
    sdk.utils.listenForEvent(sdk.events.REQUEST_FINISHED, function (data) {
      if (data.detail && ('silent' in data.detail) && data.detail.silent === true) return;
      _this.mapElement.querySelector('.loading').style.display = 'none';
    });
  };

  this.configure = function () {
    var key;

    // generic parsing of element attributes and setting config based on values
    for (key in sdk.config.map) {
      if (sdk.config.map.hasOwnProperty(key)) {
        sdk.config.map[key] = _this.mapElement.getAttribute(key) ?
                             sdk.utils.parseValue(_this.mapElement.getAttribute(key)) :
                             (_this.mapElement.getAttribute('data-' + key) ? sdk.utils.parseValue(_this.mapElement.getAttribute('data-' + key)) : sdk.config.map[key]);
      }
    }
    // tiles
    if (cookies.get('traildock-map-tiles')) {
      sdk.config.map.layers.tiles.active = cookies.get('traildock-map-tiles');
    } else {
      var tiles = _this.mapElement.getAttribute('data-tiles');
      if (tiles && sdk.config.map.layers.tiles[tiles]) {
        sdk.config.map.layers.tiles.active = tiles;
      }
    }
    // drawer configuration
    for (key in sdk.config.map.drawers) {
      if (sdk.config.map.drawers.hasOwnProperty(key)) {
        sdk.config.map.drawers[key].enabled = _this.mapElement.getAttribute('data-drawer-' + key) ?
                                      (_this.mapElement.getAttribute('data-drawer-' + key) === 'disabled' ? false : true) :
                                      sdk.config.map.drawers[key].enabled;

      }
    }
    // login url
    if (_this.mapElement.getAttribute('data-login-url')) {
      sdk.config.map.urls.login = _this.mapElement.getAttribute('data-login-url');
    }
    // width/height are special cases
    _this.mapElement.style.width  = sdk.config.map.width;
    _this.mapElement.style.height = sdk.config.map.height;
    // remove attributes from the element no longer needed, cleaning up
    _this.removeAttributes();
  };

  this.removeAttributes = function () {
    _this.mapElement.removeAttribute('width');
    _this.mapElement.removeAttribute('height');
  };

  this.addDrawerButton = function (options) {
    var elem = '<a title="' + options.title + '" ';
    if (options.action === 'drawer') {
      elem += 'href="javascript:window.traildock.map.toggleDrawer(\'' + options.name + '\');" ';
    } else {
      elem += 'href="javascript:' + options.action + '" ';
    }
    if (options.materialIcon) {
      elem += 'class="material-icons ' + options.name + '" ';
      elem += 'style="color:' + options.materialIcon.color + '">' + options.materialIcon.id + '</a>';
    } else if (options.svgIcon) {
      elem += '><img src="' + options.svgIcon + '" /></a>';
    }

    sdk.utils.append(_this.mapElement.querySelector('.drawer-buttons'), elem);
  };

  this.addDrawer = function (drawer) {
    var options = {
      title: drawer.title,
      name: drawer.name,
      action: 'drawer',
      materialIcon: {
        color: drawer.iconColor,
        id: drawer.materialIconId
      }
    };
    _this.addDrawerButton(options);
    drawer.elem = sdk.utils.append(
      _this.mapElement,
      '<div class="drawer ' + drawer.name + '" \
      style="z-index:998;width:' + drawer.width + ';right:-' + drawer.width + '"></div>'
    );
    leaflet.DomEvent.disableClickPropagation(drawer.elem);
    leaflet.DomEvent.disableScrollPropagation(drawer.elem);
    drawer.render();
  };

  this.toggleDrawer = function (name) {
    if (sdk.utils.hasClass(_this.mapElement.querySelector('.drawer.' + name), 'open')) {
      // if we've clicked on the drawer that's open, let's just close it
      return sdk.utils.removeClass(_this.mapElement.querySelector('.drawer.' + name), 'open');
    }
    sdk.utils.removeClass(_this.mapElement.querySelectorAll('.drawer'), 'open');
    sdk.utils.addClass(_this.mapElement.querySelector('.drawer.' + name), 'open');
    if (name === 'account' && !sdk.api.me.authenticated) {
      _this.mapElement.querySelector('.drawer.' + name + ' input').focus();
    }
  };

  this.addMobilePhoto = function () {
    _this.mapElement.querySelector('#mobile-photo').click();
  };

  mapElement = document.getElementById('traildock-map');
  if (mapElement) {
    this.mapElement         = mapElement;
    _this.configure();
    this.drawerButtonsElem  = sdk.utils.append(_this.mapElement, '<div class="drawer-buttons" style="z-index:999;"></div>');
    leaflet.DomEvent.disableClickPropagation(this.drawerButtonsElem);
    sdk.utils.prepend(_this.mapElement, '<div class="loading" style="width:' + sdk.config.map.width + ';height:' + sdk.config.map.height + ';z-index:2000;"></div>');
    this.init();
    var poweredBy = document.createElement('div');
    sdk.utils.addClass(poweredBy, 'powered-by');
    sdk.utils.append(poweredBy, '<span>powered by<a href="https://www.traildock.com"><img src="' + _this.traildockLogoUrl + '" /></a><span>');
    mapElement.append(poweredBy);
    poweredBy.style['z-index'] = 3000;

    sdk.utils.listenForEvent(sdk.events.LOGGED_IN, function () {
      _this.clearMarkers();
      _this.drawContent();
    });
    sdk.utils.listenForEvent(sdk.events.LOGGED_OUT, function () {
      _this.clearMarkers();
      _this.drawContent();
    });
    if (sdk.utils.isMobile()) {
      sdk.utils.addClass(mapElement, 'mobile');
    }
  }

};
