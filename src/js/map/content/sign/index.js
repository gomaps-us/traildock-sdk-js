'use strict';

var path  = require('path');
var fs    = require('fs');

module.exports = function (sdk, base) {

  var fields = [
    { name: 'description', type: 'textarea', required: true },
    { name: 'title', type: 'input', required: false },
    { name: 'files', type: 'files', required: false },
  ];

  var editMenu = '<ul>\
    <li><a href="javascript:void(0);" data-edit="edit">edit</a></li>\
    <li><a href="javascript:void(0);" data-edit="remove">remove it</a></li>\
  </ul>';

  this.name          = 'sign';
  this.apiCollection = 'signs';

  this.trailpost = {
    icon: sdk.utils.base64Encode(fs.readFileSync(path.resolve(__dirname, 'icons', 'trailpost.svg')), 'image/svg+xml'),
    fields: fields,
    help: 'Trail Post: a trail marker on or next to a trail',
    editMenu: editMenu,
  };

  this.default = {
    icon: sdk.utils.base64Encode(fs.readFileSync(path.resolve(__dirname, 'icons', 'default.svg')), 'image/svg+xml'),
    fields: fields,
    help: 'Sign: a board with information helpful to trail users or mappers',
    editMenu: editMenu,
  };

};