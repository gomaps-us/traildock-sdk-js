'use strict';

var path      = require('path');
var fs        = require('fs');
var leaflet   = require('leaflet');
var uploader  = require('./uploader');

module.exports = function (sdk) {

  var _this = this;

  this.formTemplate = fs.readFileSync(path.resolve(__dirname, 'templates', 'form.html')).toString();
  this.viewTemplate = fs.readFileSync(path.resolve(__dirname, 'templates', 'view.html')).toString();

  this.gps          = new (require('./gps'))(sdk, this);

  this.observation  = new (require('./observation'))(sdk, this);
  this.report       = new (require('./report'))(sdk, this);
  this.trailhead    = new (require('./trailhead'))(sdk, this);
  this.sign         = new (require('./sign'))(sdk, this);
  this.facility     = new (require('./facility'))(sdk, this);
  this.feature      = new (require('./feature'))(sdk, this);

  this.uploader     = new uploader(sdk);

  this.getContributable = function () {
    return [
      _this.observation,
      _this.report,
      _this.trailhead,
      _this.sign,
      _this.facility,
      _this.feature,
    ];
  };

  this.showForm = function (dataType, dataSubtype, latlng, existingData) {
    var data = {
      fields: _this.generateFieldsTemplate(this[dataType][dataSubtype].fields, existingData),
      heading: sdk.utils.ucFirst(dataSubtype === 'default' ? dataType : dataSubtype),
      icon: this[dataType][dataSubtype].icon,
      dataType: dataType
    };
    var popup = leaflet.popup()
        .setLatLng(latlng)
        .setContent(sdk.utils.populateTemplate(_this.formTemplate, data));
    popup.on('add', function (event) {
      _this.uploader.register(popup, existingData);
      _this.registerSave(popup, latlng, dataType, dataSubtype, existingData);
    });
    popup.openOn(sdk.map.mapObject);
    return popup;
  };

  this.generateFieldsTemplate = function (fields, existingData) {
    existingData = existingData ? existingData : {};
    var html = '';
    for (var i = 0; i < fields.length; i++) {
      html += '<label for="' + fields[i].name + '">' + sdk.utils.ucFirst(fields[i].name) +
              (fields[i].required ? '<span class="required">*</span>' : '') +
              '</label>';
      html += '<div>';
      if (fields[i].type === 'files') {
        html += _this.uploader.getHtml(fields[i].required);
      } else {
        html += '<' + fields[i].type;
        html += ' class="submit-data' + (fields[i].required ? ' required' : '') + '"';
        html += ' data-common-name="' + sdk.utils.ucFirst(fields[i].name) + '"';
        html += ' name="' + fields[i].name + '"';
        html += fields[i].type === 'textarea' ?
                '>' + (existingData[fields[i].name] ? existingData[fields[i].name] : '') + '</textarea>' :
                ' type="text" value="' + (existingData[fields[i].name] ? existingData[fields[i].name] : '') + '" />';
      }
      html += '</div>';
    }
    return html;
  };

  this.registerSave = function (popup, latlng, dataType, dataSubtype, existingData) {
    popup._container.querySelector('button').addEventListener('click', function () {
      this.setAttribute('disabled', 'disabled');
      var endpoint = _this[dataType].apiCollection;
      // the standard fields based on the data
      var i;
      var body = {
        geometry: {
          type: "Point",
          coordinates: [latlng.lng, latlng.lat]
        }
      };
      if (dataSubtype !== 'default') {
        body.tags = ['subtype:' + dataSubtype];
      }
      var nodes = this.parentNode.querySelectorAll('.submit-data');
      for (i = 0; i < nodes.length; i++) {
        if (nodes[i].value.trim() !== '') {
          body[nodes[i].getAttribute('name')] = nodes[i].value.trim();
        }
        if (sdk.utils.hasClass(nodes[i], 'required') && nodes[i].value.trim() === '') {
          popup._container.querySelector('.error').innerHTML = nodes[i].getAttribute('data-common-name') + ' is required';
          return;
        }
      }
      // then see if we need to add or update some files as well
      var dropFiles = popup._container.querySelector('.drop-files');
      if (dropFiles) {
        body.files  = [];
        var files   = dropFiles.querySelectorAll('img');
        for (i = 0; i < files.length; i++) {
          if (files[i].getAttribute('data-content')) {
            body.files.push(files[i].getAttribute('data-content'));
          } else {
            body.files.push(files[i].getAttribute('src'));
          }
        }
        if (sdk.utils.hasClass(dropFiles, 'required') && body.files.length === 0) {
          popup._container.querySelector('.error').innerHTML = 'at least one image/file is required';
          return;
        }
      }
      sdk.api.request((existingData ? 'PUT' : 'POST'), '/' + endpoint + (existingData ? '/' + existingData.id + '/update' : ''), body).then(function (response) {
        sdk.map.mapObject.closePopup();
        if (existingData) sdk.map.removeMarkerById(existingData.id);
        return sdk.map.addMarker(dataType, response.body);
      }).catch(function (error) {
        console.error(error);
        popup._container.querySelector('.error').innerHTML = error.body.error.message;
      });
    });
  };

  this.createView = function(type, data) {
    var subtype = sdk.utils.getTagByName('subtype', data);
    subtype     = subtype ? subtype : 'default';
    var content = sdk.api.me.authenticated ?
                  '<span class="edit-menu-trigger material-icons" href="javascript:void(0);">mode_edit</span>' :
                  '';
    var files = '';
    var i;
    if (data.files instanceof Array) {
      for (i = 0; i < data.files.length; i++) {
        var imgSrc = data.files[i];
        if (!sdk.utils.getFuzzyMimeType(imgSrc).match(/^image/)) {
          imgSrc = _this.uploader.genericPreview;
        }
        files += '<a href="' + data.files[i] + '" target="_blank"><img src="' + imgSrc + '" /></a>';
      }
    }
    var externalLinks = '';
    if (data.associations && data.associations.length > 0) {
      for (i = 0; i < data.associations.length; i++) {
        switch (data.associations[i].source) {
          case 'osm':
            externalLinks += '<a href="http://www.openstreetmap.org/' + data.associations[i].type + '/' + data.associations[i].sid + '" target="_blank">view on openstreetmap.org</a>';
            break;
          default:
            // no default
        }
      }
    }
    content += sdk.utils.populateTemplate(_this.viewTemplate, {
      icon: _this[type][subtype].icon,
      title: data.title || data.heading || data.name || '',
      body: data.description || '',
      files: files,
      id: data.id,
      timestamp: (new Date(data.createdAt)).toString(),
      username: data.createdByUsername,
      editMenu: _this[type][subtype].editMenu,
      externalLinks: externalLinks,
    });
    return {
      html: content,
      icon: {
        url: _this[type][subtype].icon,
        size: 35
      }
    };
  };

  this.edit = function (popup, type, instruction) {
    var parts = instruction.split(',');
    var body  = {
      state: 'archived'
    };
    var marker = popup._source;
    if (parts[0] === 'remove') {
      if (parts[1] && parts[1] === 'me') {
        body.attributions = {
          fixedby: sdk.api.me.username
        };
      }
      sdk.api.request('PUT', '/' + _this[type].apiCollection + '/' +
                                   marker.options.apiData.id + '/update', body).then(function (response) {
        sdk.map.removeMarkerById(marker.options.apiData.id);
      }).catch(function (error) {
        console.error(error);
      });
    } else if (parts[0] === 'edit') {
      var subtype = sdk.utils.getTagByName('subtype', marker.options.apiData);
      _this.showForm(type, (subtype ? subtype : 'default'), popup.getLatLng(), marker.options.apiData);
    }

  };

};
