'use strict';

var path  = require('path');
var fs    = require('fs');

module.exports = function (sdk, base) {

  var fields = [
    { name: 'files', type: 'files', required: true },
    { name: 'title', type: 'input', required: false },
    { name: 'description', type: 'textarea', required: false },
  ];

  var editMenu = '<ul>\
    <li><a href="javascript:void(0);" data-edit="edit">edit</a></li>\
    <li><a href="javascript:void(0);" data-edit="remove">remove it</a></li>\
  </ul>';

  this.name          = 'observation';
  this.apiCollection = 'observations';

  this.default = {
    icon: sdk.utils.base64Encode(fs.readFileSync(path.resolve(__dirname, 'icons', 'default.svg')), 'image/svg+xml'),
    fields: fields,
    help: 'Observation: something noteworthy along the trail, often accompanied with a picture',
    editMenu: editMenu,
  };

};