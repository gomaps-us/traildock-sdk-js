'use strict';

var path  = require('path');
var fs    = require('fs');

module.exports = function (sdk, base) {

  var fields = [
    { name: 'description', type: 'textarea', required: true },
    { name: 'title', type: 'input', required: false },
    { name: 'files', type: 'files', required: false },
  ];

  var editMenu = '<ul>\
    <li><a href="javascript:void(0);" data-edit="edit">edit</a></li>\
    <li><a href="javascript:void(0);" data-edit="remove">remove it</a></li>\
  </ul>';

  this.name          = 'facility';
  this.apiCollection = 'facilities';

  this.campground = {
    icon: sdk.utils.base64Encode(fs.readFileSync(path.resolve(__dirname, 'icons', 'campground.svg')), 'image/svg+xml'),
    fields: fields,
    help: 'Campground: camping allowed here',
    editMenu: editMenu,
  };

  this.toilet = {
    icon: sdk.utils.base64Encode(fs.readFileSync(path.resolve(__dirname, 'icons', 'toilet.svg')), 'image/svg+xml'),
    fields: fields,
    help: 'Toilet: for when you gotta go',
    editMenu: editMenu,
  };

  this.shelter = {
    icon: sdk.utils.base64Encode(fs.readFileSync(path.resolve(__dirname, 'icons', 'shelter.svg')), 'image/svg+xml'),
    fields: fields,
    help: 'Shelter: a place to take it if you need it',
    editMenu: editMenu,
  };

  this.default = {
    icon: sdk.utils.base64Encode(fs.readFileSync(path.resolve(__dirname, 'icons', 'default.svg')), 'image/svg+xml'),
    fields: fields,
    help: 'Facility: any other human-made resource along or near a trail such as a picnic area or a yurt',
    editMenu: editMenu,
  };

};