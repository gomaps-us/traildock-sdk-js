'use strict';

var fs   = require('fs');
var path = require('path');

module.exports = function (sdk) {

  var _this           = this;

  this.elem           = null; // set by the map
  this.name           = "settings";
  this.title          = "Map Settings";
  this.materialIconId = "settings";
  this.width          = "200px";
  this.iconColor      = "#F77502";

  this.render = function () {
    if (!_this.elem) return;
    var tileLayerOptions = '';
    for (var key in sdk.map.tileLayers) {
      if (sdk.map.tileLayers.hasOwnProperty(key)) {
        tileLayerOptions += '<div><input type="radio" onclick="window.traildock.map.drawers.settings.updateTileLayer(this);" name="tileLayer" value="' + key + '"';
        if (sdk.map.tileLayers[key].active) {
          tileLayerOptions += ' checked="checked"';
        }
        tileLayerOptions += ' /> ' + sdk.map.tileLayers[key].name + '</div>';
      }
    }
    _this.elem.innerHTML = sdk.utils.populateTemplate(
      fs.readFileSync(path.resolve(__dirname, 'content.html')),
      {
        'tileLayerOptions': tileLayerOptions
      }
    );

  };

  this.updateTileLayer = function (radio) {
    sdk.map.activateTileLayer(radio.getAttribute('value'));
  };

};