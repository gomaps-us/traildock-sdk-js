'use strict';

var fs   = require('fs');
var path = require('path');

module.exports = function (sdk) {

  var _this           = this;

  this.elem           = null; // set by the map
  this.name           = "account";
  this.title          = "Your Traildock account";
  this.materialIconId = "account_box";
  this.width          = "200px";
  this.iconColor      = "#ED0626";

  this.render = function () {
    if (!_this.elem) return;
    if (sdk.api.me.authenticated) {
      _this.renderAuthenticated();
    } else {
      _this.elem.innerHTML = fs.readFileSync(path.resolve(__dirname, 'unauthenticated.html'));
    }
    var formFields = _this.elem.querySelectorAll('input');
    for (var i = 0; i < formFields.length; i++) {
      sdk.utils.listenForEvent('keydown', _this.onFormFieldKeydown, formFields[i]);
    }
  };

  this.onFormFieldKeydown = function (event) {
    if (event.key.toLowerCase() === "enter") {
      _this.login();
    }
  };

  this.renderAuthenticated = function () {
    _this.elem.innerHTML = sdk.utils.populateTemplate(
      fs.readFileSync(path.resolve(__dirname, 'authenticated.html')),
      {
        'name': sdk.api.me.name,
        'username': sdk.api.me.username
      }
    );
  };

  this.login = function () {
    sdk.api.login(
      document.getElementById('traildock-username').value,
      document.getElementById('traildock-password').value
    ).then(function (me) {
      _this.render();
    }).catch(function (error) {
      _this.elem.querySelector('.error').innerHTML = error.body.error.message;
      console.error(error);
    });
  };

  this.logout = function () {
    sdk.api.logout().then(function () {
      _this.render();
    });
  };

};