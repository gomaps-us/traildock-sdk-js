'use strict';

var fs    = require('fs');
var path  = require('path');

module.exports = function (sdk, content) {

  var _this           = this;

  this.content        = content;

  this.elem           = null; // set by the map
  this.name           = "contribute";
  this.title          = "Contribute to this map";
  this.materialIconId = "add_location";
  this.width          = "200px";
  this.iconColor      = "#0772DE";

  this.contributable  = this.content.getContributable();

  this.placeDraggable = function (draggable, event, target) {
    var position          = sdk.utils.getMousePosition(event, target);
    draggable.style.left  = (position.offset.x - draggable.offsetWidth/2 + 5) + 'px';
    draggable.style.top   = (position.offset.y - draggable.offsetHeight/2 + 5) + 'px';
  };

  this.registerDraggableEvents = function () {
    var draggable;
    var position;
    var onMouseMoveOverMapObject = function (event) {
      position = event.layerPoint;
    };
    var onMouseMove = function (event) {
      _this.placeDraggable(draggable, event, sdk.map.mapElement);
    };
    var onMapMouseDown = function (event) {
      var latlng  = sdk.map.mapObject.layerPointToLatLng([position.x + 5, position.y + 5]);
      _this.content.showForm(
        draggable.getAttribute('data-type'),
        draggable.getAttribute('data-subtype'),
        latlng
      );
      draggable.parentNode.removeChild(draggable);
      removeListeners();
    };
    var onKeyDown = function (event) {
      if (event.key.toLowerCase() === 'escape') {
        removeListeners();
        draggable.parentNode.removeChild(draggable);
      }
    };
    var removeListeners = function () {
      sdk.map.mapElement.removeEventListener('mousemove', onMouseMove);
      sdk.map.mapElement.removeEventListener('mousedown', onMapMouseDown);
      sdk.map.mapObject.removeEventListener('mousemove', onMouseMoveOverMapObject);
      document.removeEventListener('keydown', onKeyDown);
    };
    var onDrawerMouseDown = function (event) {
      if (sdk.utils.hasClass(event.target, 'draggable-icon')) {
        draggable                   = event.target.cloneNode(true);
        draggable.style['z-index']  = 1001;
        sdk.utils.addClass(draggable, 'dragging');
        sdk.map.mapElement.appendChild(draggable);
        _this.placeDraggable(draggable, event, sdk.map.mapElement);
        sdk.map.mapElement.addEventListener('mousemove', onMouseMove, false);
        sdk.map.mapElement.addEventListener('mousedown', onMapMouseDown, false);
        sdk.map.mapObject.addEventListener('mousemove', onMouseMoveOverMapObject, false);
        document.addEventListener('keydown', onKeyDown);
      }
    };
    _this.elem.addEventListener('mousedown', onDrawerMouseDown, false);
  };

  this.render = function () {
    if (!_this.elem) return;
    if (sdk.api.me.authenticated) {
      _this.elem.innerHTML = fs.readFileSync(path.resolve(__dirname, 'content.html'));
      for (var i = 0; i < _this.contributable.length; i++) {
        var types = Object.keys(_this.contributable[i]);
        for (var ii = 0; ii < types.length; ii++) {
          if (_this.contributable[i][types[ii]].icon) {
            sdk.utils.append(
              _this.elem,
              '<div class="icon in-drawer ' + types[ii].replace(/^default$/g, _this.contributable[i].name) + '">' +
              '<img data-type="' + _this.contributable[i].name + '" data-subtype="' + types[ii] + '"' +
              '" class="draggable-icon" src="' + _this.contributable[i][types[ii]].icon +
              '" data-help="' + _this.contributable[i][types[ii]].help + '" /></div>'
            );
          }
        }

      }
      _this.registerDraggableEvents();
      _this.elem.querySelectorAll('.icon img').forEach(function (icon) {
        icon.onmouseover = _this.showIconTooltip;
      });
    } else {
      _this.elem.innerHTML = '<p class="loggedout">You must <a class="login-link" href="javascript:void(0);">log in</a> to add to the map</p>';
      _this.elem.querySelector('.login-link').addEventListener('click', function (event) {
        if (sdk.config.map.urls.login) {
          document.location = sdk.config.map.urls.login;
        } else {
          sdk.map.toggleDrawer('account');
        }
      });
    }
  };

  this.showIconTooltip = function (event) {
    var parts = this.getAttribute('data-help').split(':');
    _this.elem.querySelector('.icon-help').innerHTML = '<em>' + parts[0] + '</em><span>' + parts[1] + '</span>';
    _this.elem.querySelector('.icon-help').setAttribute('style', 'height:auto;display:block;');
    this.onmouseout = function (event) {
      _this.elem.querySelector('.icon-help').innerHTML = '';
      _this.elem.querySelector('.icon-help').setAttribute('style', '');
    };
  };

  sdk.utils.listenForEvent(sdk.events.LOGGED_IN, function () {
    _this.render();
  });
  sdk.utils.listenForEvent(sdk.events.LOGGED_OUT, function () {
    _this.render();
  });

};
