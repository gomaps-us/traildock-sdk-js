'use strict';

var fs   = require('fs');
var path = require('path');

module.exports = function (sdk) {

  var _this           = this;

  this.elem           = null; // set by the map
  this.name           = "help";
  this.title          = "Get help with using this map";
  this.materialIconId = "help";
  this.width          = "200px";
  this.iconColor      = "#0ABF10";

  this.render = function () {
    if (!_this.elem) return;
    _this.elem.innerHTML = fs.readFileSync(path.resolve(__dirname, 'content.html'));
  };

};