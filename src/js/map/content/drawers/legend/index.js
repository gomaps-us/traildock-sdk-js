'use strict';

var fs   = require('fs');
var path = require('path');

module.exports = function (sdk) {

  var _this           = this;

  this.elem           = null; // set by the map
  this.name           = "legend";
  this.title          = "Legend";
  this.materialIconId = "vpn_key";
  this.width          = "220px";
  this.iconColor      = "#3C5573";

  this.render = function () {
    if (!_this.elem) return;
    _this.elem.innerHTML = fs.readFileSync(path.resolve(__dirname, 'content.html'));
  };

};