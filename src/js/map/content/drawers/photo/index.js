'use strict';

var fs   = require('fs');
var path = require('path');

module.exports = function (sdk, content) {

  var _this           = this;

  this.content        = content;

  this.elem           = null; // set by the map
  this.name           = "photo";
  this.title          = "Take a photo at your current location";
  this.materialIconId = "add_a_photo";
  this.width          = "200px";
  this.iconColor      = "#0772DE";
  this.timeoutId      = null;

  this.render = function () {
    if (!_this.elem) return;
    var currentFile = null;
    if (sdk.api.me.authenticated) {
      _this.elem.innerHTML = fs.readFileSync(path.resolve(__dirname, 'content.html'));
      sdk.utils.listenForEvent('change', function (event) {
        _this.elem.querySelector('.drop-files .info').style.display = 'none';
        var previous = _this.elem.querySelectorAll('.drop-files span');
        if (previous) {
          previous.forEach(function (item) {
            item.parentNode.removeChild(item);
          });
        }
        _this.data.images = [];
        var files = _this.elem.querySelector('#mobile-photos').files;
        var fileProcessor = function (content) {
          if (!currentFile.type.match(/^image\//g)) return;
          var previewContainer = document.createElement('span');
          var preview = document.createElement('img');
          preview.setAttribute('title', currentFile.name);
          preview.src = content;
          _this.data.images.push(content);
          previewContainer.appendChild(preview);
          _this.elem.querySelector('.drop-files').appendChild(previewContainer);
        };
        var fileError = function (error) {
          _this.elem.querySelector('.error').innerHTML = error;
        };
        for (var i = 0; i < files.length; i++) {
          currentFile = files[i];
          sdk.utils.getFileBase64(files[i]).then(fileProcessor).catch(fileError);
        }
      }, _this.elem.querySelector('#mobile-photos'));
      sdk.utils.listenForEvent('click', function (event) {
        _this.elem.querySelector('#mobile-photos').click();
      }, _this.elem.querySelector('.drop-files'));
      _this.renderTypeSelections();
      _this.elem.querySelector('button').addEventListener('click', function (event) {
        _this.elem.querySelector('.error').innerHTML = '';
        if (_this.data.images.length === 0) {
          _this.elem.querySelector('.error').innerHTML = 'add at least one photo';
          return;
        }
        if (_this.data.type === null) {
          _this.elem.querySelector('.error').innerHTML = 'categorize your photo(s)';
          return;
        }
        sdk.map.mapElement.querySelector('.loading').style.display = 'block';
        setTimeout(_this.save, 500);
      });
    } else {
      _this.elem.innerHTML = '<p class="loggedout">You must <a class="login-link" href="javascript:void(0);">log in</a> to add to the map</p>';
      _this.elem.querySelector('.login-link').addEventListener('click', function (event) {
        if (sdk.config.map.urls.login) {
          document.location = sdk.config.map.urls.login;
        } else {
          sdk.map.toggleDrawer('account');
        }
      });
    }
  };

  this.selectType = function (type, subtype, apiCollection) {
    _this.data.collection = apiCollection;
    _this.data.subtype = subtype;
    _this.data.type = type;
    sdk.utils.removeClass(_this.elem.querySelectorAll('.type-selections img'), 'selected');
    sdk.utils.addClass(_this.elem.querySelector('.type-selections .' + subtype), 'selected');
    _this.showIconTooltip(_this.elem.querySelector('.type-selections .' + subtype).getAttribute('title'));
  };

  this.showIconTooltip = function (text) {
    clearTimeout(_this.timeoutId);
    var parts = text.split(':');
    _this.elem.querySelector('.icon-help').innerHTML = '<em>' + parts[0] + '</em><span>' + parts[1] + '</span>';
    _this.elem.querySelector('.icon-help').setAttribute('style', 'height:auto;display:block;');
    _this.timeoutId = setTimeout(function () {
      _this.elem.querySelector('.icon-help').innerHTML = '';
      _this.elem.querySelector('.icon-help').setAttribute('style', '');
    }, 5000);
  };

  this.renderTypeSelections = function () {
    var container = _this.elem.querySelector('.type-selections');
    sdk.utils.append(container, '<img class="sign" title="' + _this.content.sign.default.help + '" onclick="window.traildock.map.drawers.contribute.selectType(\'sign\',\'sign\',\'' + _this.content.sign.apiCollection + '\');" src="' + _this.content.sign.default.icon + '" />');
    sdk.utils.append(container, '<img class="trailpost" title="' + _this.content.sign.trailpost.help + '" onclick="window.traildock.map.drawers.contribute.selectType(\'sign\',\'trailpost\',\'' + _this.content.sign.apiCollection + '\');" src="' + _this.content.sign.trailpost.icon + '" />');
    sdk.utils.append(container, '<img class="trailhead" title="' + _this.content.trailhead.default.help + '" onclick="window.traildock.map.drawers.contribute.selectType(\'trailhead\',\'trailhead\',\'' + _this.content.trailhead.apiCollection + '\');" src="' + _this.content.trailhead.default.icon + '" />');
    sdk.utils.append(container, '<img class="report" title="' + _this.content.report.default.help + '" onclick="window.traildock.map.drawers.contribute.selectType(\'report\',\'report\',\'' + _this.content.report.apiCollection + '\');" src="' + _this.content.report.default.icon + '" />');
    sdk.utils.append(container, '<img class="campground" title="' + _this.content.facility.campground.help + '" onclick="window.traildock.map.drawers.contribute.selectType(\'facility\',\'campground\',\'' + _this.content.facility.apiCollection + '\');" src="' + _this.content.facility.campground.icon + '" />');
    sdk.utils.append(container, '<img class="toilet" title="' + _this.content.facility.toilet.help + '" onclick="window.traildock.map.drawers.contribute.selectType(\'facility\',\'toilet\',\'' + _this.content.facility.apiCollection + '\');" src="' + _this.content.facility.toilet.icon + '" />');
    sdk.utils.append(container, '<img class="shelter" title="' + _this.content.facility.shelter.help + '" onclick="window.traildock.map.drawers.contribute.selectType(\'facility\',\'shelter\',\'' + _this.content.facility.apiCollection + '\');" src="' + _this.content.facility.shelter.icon + '" />');
    sdk.utils.append(container, '<img class="facility" title="' + _this.content.facility.default.help + '" onclick="window.traildock.map.drawers.contribute.selectType(\'facility\',\'facility\',\'' + _this.content.facility.apiCollection + '\');" src="' + _this.content.facility.default.icon + '" />');
    sdk.utils.append(container, '<img class="water" title="' + _this.content.feature.water.help + '" onclick="window.traildock.map.drawers.contribute.selectType(\'feature\',\'water\',\'' + _this.content.feature.apiCollection + '\');" src="' + _this.content.feature.water.icon + '" />');
    sdk.utils.append(container, '<img class="feature" title="' + _this.content.feature.default.help + '" onclick="window.traildock.map.drawers.contribute.selectType(\'feature\',\'feature\',\'' + _this.content.feature.apiCollection + '\');" src="' + _this.content.feature.default.icon + '" />');
    sdk.utils.append(container, '<img class="observation" title="' + _this.content.observation.default.help + '" onclick="window.traildock.map.drawers.contribute.selectType(\'observation\',\'observation\',\'' + _this.content.observation.apiCollection + '\');" src="' + _this.content.observation.default.icon + '" />');

  };

  this.reset = function () {
    var photos = _this.elem.querySelectorAll('.drop-files span');
    photos.forEach(function (photo) {
      photo.parentNode.removeChild(photo);
    });
    _this.elem.querySelector('.drop-files .info').style.display = 'block';
    sdk.utils.removeClass(_this.elem.querySelectorAll('.type-selections img'), 'selected');
    _this.initData();
  };

  this.initData = function () {
    _this.data = {
      images: [],
      collection: null,
      type: null,
      subtype: null
    };
  };

  this.save = function () {
    if (sdk.map.gpsMarker === null) {
      _this.elem.querySelector('.error').innerHTML = 'contributing through mobile requires enabling location services';
      return;
    }
    var body = {
      geometry: {
        type: "Point",
        coordinates: [sdk.map.gpsMarker.getLatLng().lng, sdk.map.gpsMarker.getLatLng().lat]
      }
    };
    if (_this.data.type !== _this.data.subtype) {
      body.tags = ['subtype:' + _this.data.subtype];
    }
    body.files = _this.data.images;
    sdk.api.request('POST', '/' +  _this.data.collection, body).then(function (response) {
      return sdk.map.addMarker(_this.data.type, response.body);
    }).then(function () {
      _this.reset();
      sdk.map.toggleDrawer('photo');
      sdk.map.goToUserPosition();
    }).catch(function (error) {
      console.error(error);
      _this.elem.querySelector('.error').innerHTML = error.body ? error.body.error.message : error;
    });
  };

  sdk.utils.listenForEvent(sdk.events.LOGGED_IN, function () {
    _this.render();
  });
  sdk.utils.listenForEvent(sdk.events.LOGGED_OUT, function () {
    _this.render();
  });

  this.initData();

};
