'use strict';

var path  = require('path');
var fs    = require('fs');

module.exports = function (sdk, base) {

  var fields = [
    { name: 'description', type: 'textarea', required: true },
    { name: 'title', type: 'input', required: false },
    { name: 'files', type: 'files', required: false },
  ];

  var editMenu = '<ul>\
    <li><a href="javascript:void(0);" data-edit="edit">edit</a></li>\
    <li><a href="javascript:void(0);" data-edit="remove,me">I fixed this</a></li>\
    <li><a href="javascript:void(0);" data-edit="remove,else">someone else fixed this</a></li>\
    <li><a href="javascript:void(0);" data-edit="remove">just remove it</a></li>\
  </ul>';

  this.name          = 'report';
  this.apiCollection = 'reports';

  this.default = {
    icon: sdk.utils.base64Encode(fs.readFileSync(path.resolve(__dirname, 'icons', 'default.svg')), 'image/svg+xml'),
    fields: fields,
    help: 'Report: a noteable hazard, maintenance issue, or status',
    editMenu: editMenu,
  };

};
