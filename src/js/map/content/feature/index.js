'use strict';

var path  = require('path');
var fs    = require('fs');

module.exports = function (sdk, base) {

  var fields = [
    { name: 'description', type: 'textarea', required: true },
    { name: 'title', type: 'input', required: false },
    { name: 'files', type: 'files', required: false },
  ];

  var editMenu = '<ul>\
    <li><a href="javascript:void(0);" data-edit="edit">edit</a></li>\
    <li><a href="javascript:void(0);" data-edit="remove">remove it</a></li>\
  </ul>';

  this.name          = 'feature';
  this.apiCollection = 'features';

  this.water = {
    icon: sdk.utils.base64Encode(fs.readFileSync(path.resolve(__dirname, 'icons', 'water.svg')), 'image/svg+xml'),
    fields: fields,
    help: 'Water: a spring or other drinking water source',
    editMenu: editMenu,
  };

  this.default = {
    icon: sdk.utils.base64Encode(fs.readFileSync(path.resolve(__dirname, 'icons', 'default.svg')), 'image/svg+xml'),
    fields: fields,
    help: 'Feature: any other natural feature, such as an outcropping or summit',
    editMenu: editMenu,
  };

};