'use strict';

/**
 * TODO: support some alternate uploader for browsers that can't do drag and drop, also support mobile
 */

module.exports = function (sdk) {

  var _this           = this;
  this.state          = 'initial';
  this.genericPreview = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAAEsCAYAAAB5fY51AAAAAXNSR0IArs4c6QAAAAlwSFlzAAALEwAACxMBAJqcGAAAAVlpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IlhNUCBDb3JlIDUuNC4wIj4KICAgPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICAgICAgPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIKICAgICAgICAgICAgeG1sbnM6dGlmZj0iaHR0cDovL25zLmFkb2JlLmNvbS90aWZmLzEuMC8iPgogICAgICAgICA8dGlmZjpPcmllbnRhdGlvbj4xPC90aWZmOk9yaWVudGF0aW9uPgogICAgICA8L3JkZjpEZXNjcmlwdGlvbj4KICAgPC9yZGY6UkRGPgo8L3g6eG1wbWV0YT4KTMInWQAAFMVJREFUeAHtncmvJFl5xbupLoaq7pJasmVDC8SWBZaMwV54Y3kJMqNbQg3/AYPtpgRdEi28oyxLnrAs761uMQ8WEl5YjcSM5RUseouEjOSVxVDVhhqac6rig3hFvXoV+fJG5D3390nfi8jMyLjx/c6NkzfiZlU+8AABAQhAAAIQgAAEIAABCEAAAoMSeDC47uTagmXbvLQXNz8CDmAoAjaqM0NVTLH7JED/2SfNPe8rbRTyEvG5OTF6SMtze+bF7rIJ3FB5V6YS/aHn0Vb1p+lpFlsSSDKsMquXC+gl5Z8rX6X08wQE7ofAVW3038q/UX5jeoONy0ZGQGBvBMp4bVbPKf3JSMJg1z7gUdW/KV+jdHi0ThwAgTrRD+BQTnUI9Sn419rLx5Q/V55VptSnUoiVCHg05f7kvvO/yncqv6W0afk1myCxEYGEE9o1uBO5Q31P+TqlPyG5FBQEYicC7k/Xlf7Q8/K9yk8pq0+5fxEbECgBNmh67036Bvsrp70mGPHeAbHD+ybg/mOz8ojKH4SfVH5EaaPiw1AQtookwzLDtHq26he0e5uALw1rNHVZ6/86gfFzfo1YmUDCSKQuCS+I3Q+VXnpIf1xt3IMQHOJXBI7rJ7/aQCvuM2VS/6H1x5U/U3r05UtGYiUC9yPWSoeyczNLDWvnhnjj8ARsTjap7yvfqvyB0peO15TECgRGMyx/UvqTkVHWCp2rgybc/x9WLjkPyrQ8g/h25XeUNjFmEAWhdSwRqvWx7Lp/12ADutcloV/3dj9Vehbx/5T+ZMS4BGHAcF/wqOhR5fPKR5TVR7R6YpRpefleZc0geh/0qRPxjb1Bma4N68dTh/H9huo8XtZjv84/1xEE4hYB94Xj+sy8/9xt3WZVz3sGsYKJnyLRYOmh7GjhkZXDHcsdjhiPgD/k/CFWfWEXAjWD6H7kGcTXKN+n9H79mi8RCQj8BoGlIyyPxBz1vtuP+DsSgdL+XqPyGj2dtLRB1Wjry1o/P4EccTDQvA8xfG2OmAbCCdj8PKKyab1F+W3la5V+fJoRnN5O3EkAw7qTCI8hsBsBj6h8I//1Ss8c/tH02M/XiE6rxGkIYFinocd7IXCUgEdUHln9jvJrSn/B1I9tWJiWIJw2MKzTEuT9EDhKwCMq33B/qfLTyg8rfZ/L98I43wRh9KhPrnvdQK0O4ylsbrqP3mN+Pdq5V5856Wb7Sa/XF0m93T/PkPt+F7EjARx/R3C8DQInEPC5ZbOycfnrDv+ufMX0mBlEgdglMKxdqPEeCNwfAY/+awbxz7Tum/GvVTKDKAi7BIa1CzXeA4FlBGoG8ff0Nn/t4Q+VnlFkBlEQlgSGtYQW20JgdwI1g/i72sXXlf6vl5lBXMgTw1oIjM0hcAoC8xnEz2k/F5U1IcS5eB9ggXQfkNgEAnsk4HtaNinH3yo/cWvt1/8GcXrI4m4EMKy7UeE5CLQl4POuZhDfr3XPIL5c6RlFZhAF4bjAsI4jw/MQaEvgzhnEb6q5Vyt9XwvTOoY9hnUMGJ6GwEoEbE6eMXyD8rvKNynLtOpL0XqKMAEMi34Age0J1Ayif6buG8p3KJlBvIsuGNZdoPAUBDYg4JGW72H53yB+XvmXSmYQBWEeGNacBusQ2JbAfAbx73Uo/zQdjo3Lrw0fGNbwXQAAB0bA52TNIH5A619SMoM4iYRhTSBYQOCACPhmu89N38d6q9L3tR6bHvvScdjAsIaVnsIPnIBNy+bkGcQ/UHoG8Y3KmkHU6niBYY2nORX3RaBmED3C8kjLP95q0/K5O9zXHjAsqU5A4MAJeKTlGcSXKb+g/AvlkDOIGJaUJyDQAYH5DOI/6Hg9i+gYagYRw7otOn8h0AMBn6+eQbRJ+XtaX1QONYOIYUlxAgIdEfB9K6fvY71N+XXlq6bHvt8VHRhWtLwUF0pgPoPomcP/Uv6+0jOK0V97wLCkMAGBTgnMZxC/qRr8na3oGUQMq9OeymFDYCJQM4j+RR5/K/6DytgZRAxrUp0FBDomMJ9B/EfV8XdTLTauqHM8qpiOOxyHDoHTEvC5XDOIf6X1zyr9Pz9EmRaGJUUJCIQQmM8gvks1Pac8p7Rp+bXuA8PqXkIKgMARAjWD+As9+8fKj06vRpzrEUUckYsHEICACdTXG96t9fpyafejLAyLzj06AV8uJUaZ02+pOM8gRgSGFSEjRexI4Kre96Ppvb5hnRj/r6L8hdKIwLAiZKSIhQRsTv4qgL9k+enpvT6pPdryaz3ldPjHLvw/PHCeH4tn/Rdq6HtBTf9Y6c52Z8erx37d2znqfbcf8Xc0AqW/p/7/U9mTSd3PsUb2+boxN1pnpV4I+KT3yMOzaW9RPqV8XPmYspcRiU33YWWZr1azI6FQ1+DO55HTD6elH89rq8c/0fP+dV0v631aJQYmYHPyaMThD3B/b+nQw33Xl7CPKp9XPqKsPq7VW1GPo/o8I6ySl+WoBGxWNgAbl+9p+QTvKWxMwwSGNYzUFHoPAj7p/V8Qz0fl99h885d8nDba+P//6k7SGNadRHg8MoHeRiu9He+p+1YvNxdPXSg7gAAE+ieAYfWvIRVAYBgCGNYwUlMoBPongGH1ryEVQGAYAhjWMFJTKAT6J4Bh9a8hFUBgGAIY1jBSUygE+ieAYfWvIRVAYBgCGNYwUlMoBPongGH1ryEVQGAYAhjWMFJTKAT6J4Bh9a8hFUBgGAIY1jBSUygE+ieAYfWvIRVAYBgCGNYwUlMoBPongGH1ryEVQGAYAhjWMFJTKAT6J4Bh9a8hFUBgGAIY1jBSUygE+ieAYfWvIRVAYBgCGNYwUlMoBPongGH1ryEVQGAYAhjWMFJTKAT6J8DvEq6vYS8/1rk+mT5bHO63AbeUCcNaj76Nqn6xd71Waak1AV+l2LQwrtaktX8MawXIasJGVZ36HNzXgb5CK9fVxtWpndJ4hWbHbQLDaq99deSzauqy8gmlTYvon4DN6hnlJeU1ZWmtVaIFAQyrBdWj+6xO/HE9/eTRl3jUOYELOv4PTTVc1LK07ryswz18ZgnbauMOfFPpEdV7pqZuaFmXhyz7ZmEtHTVqttbWnGhEAMNqBJbdQgAC+yeAYe2f6XyPHkGZcd3r8GtnlP4UJvtnYC0dzyqtsbW25kQjAtzDagR2ttvqwL4x6/Dlw/lba/zpncAVFWCzKm1L697rOtjjx7DaS+NO7NGUZ5F8Y/ZppWcMif4JWNMXpjKsMYbVWFMMqzHgafdlWu7U7uDVyddpnVZaEqjLQMyqJeVp3xjWCpCnJtyhy7jWa5WWWhPwzCCxEgEMayXQs2b4JJ7BYBUCSwgwS7iEFttCAAKbEsCwNsVP4xCAwBICGNYSWmwLAQhsSgDD2hQ/jUMAAksIYFhLaLEtBCCwKQEMa1P8NA4BCCwhgGEtocW2EIDApgQwrE3x0zgEILCEAIa1hBbbQgACmxLAsDbFT+MQgMASAhjWElpsCwEIbEoAw9oUP41DAAJLCGBYS2ixLQQgsCkBDGtT/DQOAQgsIYBhLaHFthCAwKYEMKxN8dM4BCCwhACGtYQW20IAApsSwLA2xU/jEIDAEgIY1hJabAsBCGxKAMPaFD+NQwACSwhgWEtosS0EILApAQxrU/w0DgEILCGAYS2hxbYQgMCmBDCsTfHTOAQgsIQAhrWEFttCAAKbEsCwNsVP4xCAwBICGNYSWmwLAQhsSgDD2hQ/jUMAAksIYFhLaLEtBCCwKYGHNm19zMYfHLPs2KpfjK3sAAvDsNYTxUblvLlek7S0AgFfpdi0MK4VYGNYK0BWEzaq6tTntA73dbi3buW6Grg6NVIat25z6P1z4rSXvzryWTV1WfmE0qZF9E/AZvWM8pLymrK01irRggCG1YLq0X1WJ/64nn7y6Es86pzABR3/h6YaLmpZWnde1uEePrOEbbVxB/Y9K4+o3jM1dUPLujxk2TcLa+moUbO1tuZEIwIYViOw7BYCENg/AQxr/0zne/QIyozrXodfO6P0pzDZPwNr6XhWaY2ttTUnGhHgHlYjsLPdVgf2jVmHLx/O31rjT+8ErqgAm1VpW1r3XtfBHj+G1V4ad2KPpjyL5BuzTys9Y0j0T8CavjCVYY0xrMaaYliNAU+7L9Nyp3YHr06+Tuu00pJAXQZiVi0pT/vGsFaAPDXhDl3GtV6rtNSaAP9yoTXh2f4xrBmMlVb5JF4JNM3kEWCWME9TKoJALAEMK1ZaCoNAHgEMK09TKoJALAEMK1ZaCoNAHgEMK09TKoJALAEMK1ZaCoNAHgEMK09TKoJALAEMK1ZaCoNAHgEMK09TKoJALAEMK1ZaCoNAHgEMK09TKoJALAEMK1ZaCoNAHgEMK09TKoJALAEMK1ZaCoNAHgEMK09TKoJALAEMK1ZaCoNAHgEMK09TKoJALAEMK1ZaCoNAHgEMK09TKoJALAEMK1ZaCoNAHgEMK09TKoJALAEMK1ZaCoNAHgEMK09TKoJALAF+l3B9af3rz0QOAX5nckUtMaz1YNuonPxS8HrM12jJVyn1q95rtDd0GxjWOvLbqKpTn9M63Nfh3rqV62rg6tRIady6zaH3z4nTXv7qyGfV1GXlE0qbFtE/AZvVs8qnlNeUpbVWiRYEMKwWVI/uszqxzerJoy/xqHMCFyZNPXq+qCytOy/rcA+fWcK22rgD+56VR1QeWTluKOvykGXfLKylo0bN1tqaE40IYFiNwLJbCEBg/wQwrP0zne/RIygz9r2OZ6YXzmjpT2GyfwbW0uH7WNbYWltzohEB7mE1AjvbbXXgS9Nzvnw4P3ud1X4JXNGh26xK29K634oO/MgxrPYCuRN7NOVZJN+YfVrpGUOifwLW9IWpDGuMYTXWFMNqDHjafZmWO7U7eHXydVqnlZYE6jIQs2pJedo3hrUC5KkJd+gyrvVapaXWBPiXC60Jz/aPYc1grLTKJ/FKoGkmjwCzhHmaUhEEYglgWLHSUhgE8ghgWHmaUhEEYglgWLHSUhgE8ghgWHmaUhEEYglgWLHSUhgE8ghgWHmaUhEEYglgWLHSUhgE8ghgWHmaUhEEYglgWLHSUhgE8ghgWHmaUhEEYglgWLHSUhgE8ghgWHmaUhEEYglgWLHSUhgE8ghgWHmaUhEEYglgWLHSUhgE8ghgWHmaUhEEYglgWLHSUhgE8ghgWHmaUhEEYglgWLHSUhgE8ghgWHmaUhEEYglgWLHSUhgE8ghgWHmaUhEEYglgWLHSUhgE8ghgWHmaUhEEYglgWLHSUhgE8ghgWHmaUhEEYglgWLHSUhgE8ghgWHmaUhEEYgk8FFvZ4Rb24OEeGke2A4EXd3gPb9mRAIa1I7gd3majct7c4b285XAJ+CrFpoVxraARhrUCZDVho6pOfU7rcF+He+tWrquBq1MjpXHrNofePydOe/mrI59VU5eVTyhtWkT/BGxWzyqfUl5TltZaJVoQwLBaUD26z+rENqsnj77Eo84JXJg09ej5orK07ryswz18ZgnbauMO7HtWHlF5ZOW4oazLQ5Z9s7CWjho1W2trTjQigGE1AstuIQCB/RPAsPbPdL5Hj6DM2Pc6npleOKOlP4XJ/hlYS4fvY1lja23NiUYEuIfVCOxst9WBL03P+fLh/Ox1VvslcEWHbrMqbUvrfis68CPHsNoL5E7s0ZRnkXxj9mmlZwyJ/glY0xemMqwxhtVYUwyrMeBp92Va7tTu4NXJ12mdVloSqMtAzKol5WnfGNYKkKcm3KHLuNZrlZZaE+BfLrQmPNs/hjWDsdIqn8QrgaaZPALMEuZpSkUQiCWAYcVKS2EQyCOAYeVpSkUQiCWAYcVKS2EQyCOAYeVpSkUQiCWAYcVKS2EQyCOAYeVpSkUQiCWAYcVKS2EQyCOAYeVpSkUQiCWAYcVKS2EQyCOAYeVpSkUQiCWAYcVKS2EQyCOAYeVpSkUQiCWAYcVKS2EQyCOAYeVpSkUQiCWAYcVKS2EQyCOAYeVpSkUQiCWAYcVKS2EQyCOAYeVpSkUQiCWAYcVKS2EQyCOAYeVpSkUQiCWAYcVKS2EQyCOAYeVpSkUQiCWAYcVKS2EQyCOAYeVpSkUQiCWAYcVKS2EQyCOAYeVpSkUQiCWAYcVKS2EQyCOAYeVpSkUQiCWAYcVKS2EQyCOAYeVpSkUQiCWAYcVKS2EQyCOAYeVpSkUQiCWAYcVKS2EQyCOAYeVpSkUQiCWAYcVKS2EQyCOAYeVpSkUQiCWAYcVKS2EQyCOAYeVpSkUQiCWAYcVKS2EQyCOAYeVpSkUQiCWAYcVKS2EQyCOAYeVpSkUQiCWAYcVKS2EQyCOAYeVpSkUQiCWAYcVKS2EQyCOAYeVpSkUQiCWAYcVKS2EQyCOAYeVpSkUQiCWAYcVKS2EQyCOAYeVpSkUQiCWAYcVKS2EQyCOAYeVpSkUQiCWAYcVKS2EQyCOAYeVpSkUQiCWAYcVKS2EQyCOAYeVpSkUQiCWAYcVKS2EQyCOAYeVpSkUQiCWAYcVKS2EQyCOAYeVpSkUQiCWAYcVKS2EQyCOAYeVpSkUQiCWAYcVKS2EQyCOAYeVpSkUQiCXwUGxlxxf24PRSLY/fklcgcJgE3HdfVA7Xh0c0rGtTH7x5mH2Ro4LAiQRsVo7qy7cfDfB3NMPyJ9Kjk65ntSzhB5CaEoMIuB/brNyXhxpljWJYJerDEvh5JUYlCET3BNyv3acd1cdvPwr9O4phlXwW9ZF6wBICEOiLwGiGZXUYXfXVRznaexMYYmRVCEY0rKEELqFZQiCBQNr3sJj5S+iV1LBPAlHnRJJhXZXKP5qU5rJvn12effVIoM6B/9HB+9yIiDMRVTzwgOu4ofxt5Z8of6FMMmOVQ0DgvgnYrPy1B9/y+RflV5U+R7ofbaXcz3EdFumlyq8o/1RJQGB0As8JwJuVP1fWOdI1kxTDsggeUfkT5GXKp5SPKx9TMtISBGIYAj4HfBn4GeVlpc2qzg2t9h1JhmUl5sJ4OHyub3k4egjsRMD3rK5P75yfEzvtjDe1JWATTrk315YUe08m4HMgbUCSV9CsB8aJNauNVQicRKBmCU/ajtchAAEIQAACEIAABCAAAQhAAAI9EPglTnT14COVTiQAAAAASUVORK5CYII=';
  this.zeroState      = '<div class="info">Drag and drop to attach images/files</div>';

  this.getHtml = function (required) {
    var html = '<div class="drop-files' + (required ? ' required' : '') + '">';
    if ((typeof FileReader !== 'undefined') &&
        ('draggable' in document.createElement('span')) &&
        (!!window.FormData)) {
      html += _this.zeroState;
      _this.state = 'active';
    } else {
      html += '<div class="info">Your browser doesn\'t support drag and drop</div>';
      _this.state = 'disabled';
    }
    html += '</div>';
    return html;
  };

  this.addFile = function (dropFiles, src, content) {
    var srcUrl;
    if (typeof src === 'string') {
      srcUrl = src;
      src = {
        name: src.substr(src.lastIndexOf('/') + 1),
        type: sdk.utils.getFuzzyMimeType(src),
      };
    }
    var file = document.createElement('span');
    var x = document.createElement('a');
    x.setAttribute('href', 'javascript:void(0);');
    sdk.utils.addClass(x, 'material-icons');
    x.innerHTML = 'highlight_off';
    file.appendChild(x);
    var preview = document.createElement('img');
    preview.setAttribute('title', src.name);
    if (src.type.match(/^image\//g)) {
      preview.src = content ? content : srcUrl;
    } else {
      preview.src = _this.genericPreview;
      preview.setAttribute('data-content', content);
    }
    file.appendChild(preview);
    dropFiles.appendChild(file);
    x.addEventListener('click', function (event) {
      file.remove();
      if (dropFiles.querySelectorAll('img').length === 0) {
        dropFiles.querySelector('.info').style.display = "block";
      }
    });
    dropFiles.querySelector('.info').style.display = "none";
  };

  this.register = function (popup, existingData) {
    if (_this.state === 'disabled') return;
    var dropFiles           = popup._container.querySelector('.drop-files');
    dropFiles.ondragover    = function () { sdk.utils.addClass(this, 'hover'); return false; };
    dropFiles.ondragend     = function () { sdk.utils.removeClass(this, 'hover'); return false; };
    dropFiles.ondragleave   = function () { sdk.utils.removeClass(this, 'hover'); return false; };
    dropFiles.ondrop        = function (event) {
      sdk.utils.removeClass(this, 'hover');
      event.preventDefault();
      var file = event.dataTransfer.files[0];
      sdk.utils.getFileBase64(file).then(function (content) {
        _this.addFile(dropFiles, file, content);
      }).catch(function (error) {
        popup._container.querySelector('.error').innerHTML = error;
      });
    };
    if (existingData && existingData.files) {
      for (var i = 0; i < existingData.files.length; i++) {
        _this.addFile(dropFiles, existingData.files[i]);
      }
    }
  };

};