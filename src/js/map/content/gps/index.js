'use strict';

var fs    = require('fs');
var path  = require('path');

module.exports = function (sdk) {
  this.icon       = sdk.utils.base64Encode(fs.readFileSync(path.resolve(__dirname, 'icon.svg')), 'image/svg+xml');
  this.iconFixed  = sdk.utils.base64Encode(fs.readFileSync(path.resolve(__dirname, 'icon-fixed.svg')), 'image/svg+xml');
  this.layer      = require('./layer');
};