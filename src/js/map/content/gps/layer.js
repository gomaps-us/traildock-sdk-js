'use strict';

var leaflet = require('leaflet');
var fs      = require('fs');
var path    = require('path');
var utils   = require('../../../utils');

module.exports = leaflet.Layer.extend({
  initialize: function (latlng) {
    this._latlng = leaflet.latLng(latlng);
  },
  onAdd: function (map) {
    this._zoomAnimated = this._zoomAnimated && map.options.markerZoomAnimation;
    if (this._zoomAnimated) {
      map.on('zoomanim', this._animateZoom, this);
    }
    this._container           = document.createElement('div');
    this._container.innerHTML = fs.readFileSync(path.resolve(__dirname, 'icon.svg')).toString();
    this._container           = this._container.firstChild;
    this._container.setAttribute('width', '40px');
    this._container.setAttribute('height', '40px');
    this._container.style['margin-left'] = '-20px';
    this._container.style['margin-top'] = '-20px';
    this._container.style.width = '40px';
    this._container.style.height = '40px';
    this._container.style['z-index'] = 1000;
    utils.addClass(this._container, 'leaflet-marker-icon');
    utils.addClass(this._container, 'leaflet-zoom-animated');
    this.getPane().appendChild(this._container);
    this.update();
  },
  getEvents: function () {
    return {
      zoom: this.update,
      viewreset: this.update
    };
  },
  update: function () {
    if (this._container) {
      var pos = this._map.latLngToLayerPoint(this._latlng).round();
      leaflet.DomUtil.setPosition(this._container, pos);
    }
    return this;
  },
  onRemove: function (map) {
    leaflet.DomUtil.remove(this._container);
    this._container = null;
    if (this._zoomAnimated) {
      map.off('zoomanim', this._animateZoom, this);
    }
  },
  getLatLng: function () {
    return this._latlng;
  },
  setLatLng: function (latlng) {
    var oldLatLng = this._latlng;
    this._latlng = leaflet.latLng(latlng);
    this.update();
    // @event move: Event
    // Fired when the marker is moved via [`setLatLng`](#marker-setlatlng) or by [dragging](#marker-dragging). Old and new coordinates are included in event arguments as `oldLatLng`, `latlng`.
    return this.fire('move', {oldLatLng: oldLatLng, latlng: this._latlng});
  },
  _animateZoom: function (opt) {
    var pos = this._map._latLngToNewLayerPoint(this._latlng, opt.zoom, opt.center).round();
    leaflet.DomUtil.setPosition(this._container, pos);
  }
});
