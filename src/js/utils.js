'use strict';

var TraildockUtils = function () {

  var _this = this;

  this.isMobile = function () {
    if(/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(window.navigator.userAgent)) return true;
    return false;
  };

  this.getIEVersion = function () {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
      // IE 10 or older
      return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
      // IE 11
      var rv = ua.indexOf('rv:');
      return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
      // Edge (IE 12+)
      return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    }

    // other browser
    return false;
  };

  this.loadStyle = function(url) {
    if (!document) {
      throw 'Cannot load a style because document is undefined';
    }
    return new Promise(function (resolve, reject) {
      var link      = document.createElement('link');
      link.type     = 'text/css';
      link.rel      = 'stylesheet';
      link.onload   = function () { resolve(); };
      link.onerror  = function (msg) { reject(msg); };
      link.href     = url;
      document.getElementsByTagName('head')[0].appendChild(link);
    });
  };

  this.toString = function (value) {
    if (!value) return "";
    if (value.toString) return value.toString();
    return value;
  };

  this.append = function(to, childHtml) {
    if (!document) {
      throw 'Cannot append because document is undefined';
    }
    var div = document.createElement('div');
    div.innerHTML = childHtml;
    return to.appendChild(div.children[0]);
  };

  this.prepend = function(to, childHtml) {
    if (!document) {
      throw 'Cannot prepend because document is undefined';
    }
    var div = document.createElement('div');
    div.innerHTML = childHtml;
    if (to.firstChild instanceof Node) {
      return to.insertBefore(div.children[0], to.firstChild);
    } else {
      return to.appendChild(div.children[0]);
    }
  };

  this.hasClass = function (elem, className) {
    var classes = _this.toString(elem.getAttribute('class')).split(' ');
    if (classes.indexOf(className) >= 0) return true;
    return false;
  };

  this.addClass = function(to, className) {
    if (to instanceof NodeList) {
      for (var i = 0; i < to.length; i++) {
        _this.addClass(to[i], className);
      }
      return;
    }
    if (!_this.hasClass(to, className)) {
      to.setAttribute('class', ((to.getAttribute('class') ? to.getAttribute('class') : '') + ' ' + className).trim());
    }
  };

  this.removeClass = function(from, className) {
    var i;
    if (from instanceof NodeList) {
      for (i = 0; i < from.length; i++) {
        _this.removeClass(from[i], className);
      }
      return;
    }
    if (_this.hasClass(from, className)) {
      var existing    = from.getAttribute('class').split(' ');
      var newClasses  = '';
      for (i = 0; i < existing.length; i++) {
        if (existing[i].toLowerCase() !== className.toLowerCase()) {
          newClasses += existing[i] + ' ';
        }
      }
      newClasses = newClasses.trim();
      from.setAttribute('class', newClasses);
    }
  };

  this.dispatchEvent = function (name, data, elem) {
    if (!elem) elem = window;
    if (elem) {
      try {
        elem.dispatchEvent(new CustomEvent(name, { detail: data }));
      } catch (err) {
        var event = document.createEvent("Event");
        event.initEvent(name, true, true);
        elem.dispatchEvent(event);
      }
    }
  };

  this.listenForEvent = function (name, callback, elem) {
    if (!elem) elem = window;
    if (elem) {
      if (elem.addEventListener) {
        elem.addEventListener(name, callback);
      } else if (elem.attachEvent) {
        elem.attachEvent('on' + name, callback);
        elem.attachEvent(name, callback);
      }
    }
  };

  this.populateTemplate = function (template, values) {
    if (!(template instanceof String)) template = template.toString();
    template = template.replace(/\{\{([^\}]+)\}\}/g, function (match, $1) {
      var key = $1.trim();
      return values[key] ? values[key] : '';
    });
    return template;
  };

  this.getFileBase64 = function (file) {
    return new Promise(function (resolve, reject) {
      var reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = function () {
        return resolve(reader.result);
      };
      reader.onerror = function (error) {
        return reject('error processing file');
      };
    });
  };

  this.base64Encode = function (item, contentType) {
    return 'data:' + contentType + ';base64,' + window.btoa(item.toString());
  };

  this.getFuzzyMimeType = function (uri) {
    var ext = uri.substr(uri.lastIndexOf('.') + 1);
    if (['jpg','jpeg','bmp','png','tif'].indexOf(ext) >= 0) {
      return 'image/' + ext;
    } else {
      return 'application/octet-stream';
    }
  };

  // Returns a function, that, as long as it continues to be invoked, will not
  // be triggered. The function will be called after it stops being called for
  // N milliseconds. If `immediate` is passed, trigger the function on the
  // leading edge, instead of the trailing.
  this.debounce = function (func, wait, immediate) {
    var timeout;
    return function () {
      var context = this, args = arguments;
      clearTimeout(timeout);
      timeout = setTimeout(function() {
        timeout = null;
        if (!immediate) func.apply(context, args);
      }, wait);
      if (immediate && !timeout) func.apply(context, args);
    };
  };

  this.parseValue = function (value) {
    if (value === 1 || value === "1" || value === true || value === "true" || value === "yes") return true;
    if (value === 0 || value === "0" || value === false || value === "false" || value === "no") return false;
    return value;
  };

  this.parseUrlParams = function (params) {
    var parsed = {};
    var values = params.split('&');
    for (var i = 0; i < values.length; i++) {
      parsed[values[i].split('=')[0]] = values[i].split('=')[1];
    }
    return parsed;
  };

  this.getMousePosition = function (evt, target) {

    var pageX = evt.pageX;
    var pageY = evt.pageY;
    if (pageX === undefined) {
      pageX = evt.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
      pageY = evt.clientY + document.body.scrollTop + document.documentElement.scrollTop;
    }

    var rect = target ? target.getBoundingClientRect() : evt.target.getBoundingClientRect();
    var offsetX = evt.clientX - rect.left;
    var offsetY = evt.clientY - rect.top;

    return {
      client: { x: evt.clientX, y: evt.clientY }, // relative to the viewport
      screen: { x: evt.screenX, y: evt.screenY }, // relative to the physical screen
      offset: { x: offsetX,     y: offsetY },     // relative to the event target
      page:   { x: pageX,       y: pageY }        // relative to the html document
    };
  };

  this.ucFirst = function (string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  };

  this.getTagByName = function (name, data) {
    if (data.tags) {
      for (var i = 0; i < data.tags.length; i++) {
        if (data.tags[i].indexOf(':') >= 0) {
          var parts = data.tags[i].split(':');
          if (parts[0] === name) return parts[1];
        }
      }
    }
  };

};

module.exports = new TraildockUtils();
