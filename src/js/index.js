'use strict';

var TraildockSdk = function () {
  this.config = require('./config');
  this.events = require('./events');
  this.utils  = require('./utils');
  this.api    = new (require('./api'))(this);
  this.map    = new (require('./map'))(this);
};

if (window) {
  window.traildock = new TraildockSdk();
  module.exports = window.traildock;
} else {
  module.exports = new TraildockSdk();
}