'use strict';

module.exports = {
  REQUEST_STARTED: 'traildock-api-request-started',
  REQUEST_FINISHED: 'traildock-api-request-finished',
  LOGGED_IN: 'traildock-api-logged-in',
  LOGGED_OUT: 'traildock-api-logged-out'
};