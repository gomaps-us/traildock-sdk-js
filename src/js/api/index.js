'use strict';

var cookies   = require('browser-cookies');
var urlutil   = require('url');
var http      = require('http');

module.exports = function (sdk) {

  var _this = this;
  this.url  = null;

  this.me  = {
    authenticated: false,
    id: cookies.get('traildock-member-id') ? cookies.get('traildock-member-id') : null,
    token: cookies.get('traildock-member-token') ? cookies.get('traildock-member-token') : null,
    username: cookies.get('traildock-member-username') ? cookies.get('traildock-member-username') : null,
    name: cookies.get('traildock-member-name') ? cookies.get('traildock-member-name') : null,
    avatar: cookies.get('traildock-member-avatar') ? cookies.get('traildock-member-avatar') : null
  };

  this.setUrl = function (url) {
    _this.url = urlutil.parse(url);
  };

  this.init = function (url) {
    _this.setUrl(url ? url : sdk.config.api.url);
    return _this.getMe().then(function (me) {
      if (me.id) _this.me.authenticated = true;
      return me;
    }).catch(function () {
      return null;
    });
  };

  this.login = function (usernameEmail, password) {
    var body = {
      password: password
    };
    if (usernameEmail.indexOf('@') >= 0) {
      body.email = usernameEmail;
    } else {
      body.username = usernameEmail;
    }
    _this.me.token = null;
    return _this.request('POST', '/members/login', body).then(function (response) {
      if (response.data.statusCode === 200) {
        _this.me.authenticated  = true;
        _this.me.id             = response.body.userId;
        _this.me.token          = response.body.id;
        return response.body.ttl;
      } else {
        if (response.body.error) {
          throw response.body.error.messsge;
        } else {
          throw 'error logging in';
        }
      }
    }).then(function (ttl) {
      sdk.utils.dispatchEvent(sdk.events.LOGGED_IN);
      return _this.getMe(ttl);
    }).catch(function (error) {
      throw error;
    });
  };

  this.logout = function () {
    return _this.request('POST', '/members/logout').then(function () {
      _this.me.authenticated  = false;
      _this.me.token          = null;
      _this.me.id             = null;
      _this.me.username       = null;
      _this.me.name           = null;
      _this.me.avatar         = null;
      cookies.erase('traildock-member-token');
      cookies.erase('traildock-member-id');
      cookies.erase('traildock-member-username');
      cookies.erase('traildock-member-name');
      cookies.erase('traildock-member-avatar');
      sdk.utils.dispatchEvent(sdk.events.LOGGED_OUT);
    });
  };

  this.getMe = function(ttl) {
    return this.request('GET', '/members/me').then(function (response) {
      _this.me.id         = response.body.id;
      _this.me.name       = response.body.firstName;
      _this.me.username   = response.body.username;
      _this.me.avatar     = response.body.avatarUrl;
    }).then(function () {
      _this.updateCookies(ttl);
      return _this.me;
    }).catch(function (error) {
      throw error;
    });
  };

  this.updateCookies = function (ttl) {
    var expires = new Date();
    if (ttl) {
      expires.setSeconds(expires.getSeconds() + ttl);
      cookies.set('traildock-member-token', _this.me.token, { expires: expires });
    } else {
      expires = 0;
    }
    cookies.set('traildock-member-id', _this.me.id, { expires: expires });
    cookies.set('traildock-member-username', _this.me.username, { expires: expires });
    cookies.set('traildock-member-name', _this.me.name, { expires: expires });
    cookies.set('traildock-member-avatar', _this.me.avatar, { expires: expires });
  };

  this.request = function (method, path, body, eventOptions) {
    sdk.utils.dispatchEvent(sdk.events.REQUEST_STARTED, eventOptions);
    return new Promise(function (resolve, reject) {
      if (_this.me.token) {
        path += path.indexOf('?') >=0 ? '&' : '?';
        path += 'access_token=' + _this.me.token;
      }
      var options = {
        method: method,
        protocol: _this.url.protocol,
        hostname: _this.url.hostname,
        port: _this.url.port,
        path: path,
        headers: {}
      };
      if (body) {
        options.headers['Content-Type'] = 'application/json';
      }
      var req = http.request(options, function (res) {
        sdk.utils.dispatchEvent(sdk.events.REQUEST_FINISHED, eventOptions);
        var resBody = '';
        res.on('data', function (data) {
          resBody += data;
        });
        res.on('end', function () {
          try {
            resBody = JSON.parse(resBody);
          } catch (e) {}
          if (res.statusCode >= 400) {
            return reject({ data: res, body: resBody });
          } else {
            return resolve({ data: res, body: resBody });
          }
        });
      });
      req.on('error', function (error) {
        sdk.utils.dispatchEvent(sdk.events.REQUEST_FINISHED, eventOptions);
        return reject({ data: error, body: { error: { message: error.message }}});
      });
      if (body) {
        req.write(JSON.stringify(body));
      }
      req.end();
    });
  };

};
